<?php
namespace Kiwi\Newsletter\Fe;

/**
 * Description of ConfirmPresenter
 *
 * @author basnik
 */
class ConfirmPresenter extends \Nette\Application\UI\Presenter{
	
	/** @var Kiwi\Newsletter\NewsletterService */
	protected $service;
	
	/**
	 * Used for DI.
	 * @param \Kiwi\Newsletter\NewsletterService $service
	 */
	public function injectConfirmPresenter(\Kiwi\Newsletter\NewsletterService $service){
		$this->service = $service;
	}

	public function actionConfirmSubscribe($hash){
		
		$address = $this->service->getAddressByHash($hash);
		if($address === NULL){
			$this->view = 'addressNotFound';
			return;
		}
		
		$this->template->address = $address;
		
		// already confirmed? only display result
		if($address->isConfirmed){
			return;
		}
		
		// set address is confirmed
		$address->setIsConfirmed(1);
		$this->service->saveAddress($address);
	}
	
	public function actionCancelSubscribe($hash){
		
		$address = $this->service->getAddressByHash($hash);
		if($address === NULL){
			$this->view = 'addressNotFound';
			return;
		}
		
		$this->template->address = $address;
		
		// remove address
		$this->service->removeAddress($address);
	}
	
}

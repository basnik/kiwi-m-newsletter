<?php
namespace Kiwi\Newsletter\Be;

use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;
use Kiwi\Wysiwyg;
use Kiwi\Newsletter\Entities\Newsletter;
use Nette\Utils\DateTime;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

/**
 * Description of NewsletterManagementControl
 *
 * @author basnik
 */
class NewsletterComposeControl extends \Nette\Application\UI\Control {
	
	/** @var \Kiwi\Newsletter\NewsletterService */
	protected $service;
	
	/** @var \Kiwi\Services\FilesystemService */
	protected $filesystem;
	
	/** @var INewsletterGridFactory */
	protected $gridFactory;
	
	/** @var int */
	protected $editedId;
	
	public function __construct(\Kiwi\Newsletter\NewsletterService $service, \Kiwi\Services\FilesystemService $filesystem,
			INewsletterGridFactory $gridFactory, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) 
	{
		
		parent::__construct($parent, $name);
		
		$this->service = $service;
		$this->filesystem = $filesystem;
		$this->gridFactory = $gridFactory;
	}

	
	public function render(){
		
		
		$this->template->setFile(__DIR__.'/../templates/components/newsletterCompose.latte');
		$this->template->render();
	}
	
	public function handleEdit($id=0){
		
		$this->editedId = $id;
		$this->template->edit = TRUE;
		
		if($id > 0){
			$message = $this->service->getMessage($id);
			$attachments = array();
			if(isset($message->attachments) && count($message->attachments) > 0){
				foreach($message->attachments as $attachmentFile){
					$attachments[] = $attachmentFile->id;
				}
			}
			$this['editForm']->setDefaults(array(
				'editId' => $message->id,
				'subject' => $message->subject,
				'body' => $message->body,
				'attachments' => $attachments
			));

			$this['editForm']->addSubmit('send', 'Odeslat')->getControlPrototype()
					->addAttributes(array(
						'data-confirm' => 'Opravdu rozeslat newsletter?',
						'style' => 'margin-left: 30px;'
					));
		}
	}
	
	public function handleDelete($id){
		
		$message = $this->service->getMessage($id);
		$this->service->removeMessage($message);
		$this->flashMessage(sprintf('Zpráva %s byla odstraněna.', $message->subject), 'success');
		$this->redirect('this');
	}
	
	protected function createComponentNewsletterGrid(){
		return $this->gridFactory->create();
	}
	
	protected function createComponentEditForm(){
		
		$form = new Form();
		
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addText('subject', 'Předmět')
				->setRequired('Zadejte předmět');
		$form->addWysiwyg('body', 'Tělo zprávy')
				->setRequired('Zadejte tělo zprávy')
				->setToolbar(Wysiwyg::TOOLBAR_BASIC)
				->showCodeView();
		$form->addKiwiFiles('attachments', 'Přílohy');
		$form->addHidden('editId', 0);
		
		$form->addSubmit('save', 'Uložit (neodeslat)');
		$form->addSubmit('cancel', 'Zrušit')
				->setValidationScope(FALSE)
				->getControlPrototype()->addAttributes(array('data-confirm' => 'Opravdu zrušit? Změny nebudou uloženy!'));
		
		$form->addProtection();
		
		$form->onError[] = $this->composeFormError;
		$form->onSuccess[] = $this->proccessComposeForm;
		
		return $form;
	}
	
	public function composeFormError(){
		// just to show the form
		$this->template->edit = TRUE;
	}
	
	public function proccessComposeForm(Form $form, $values){
		
		 if($form['save']->isSubmittedBy() || (isset($form['send']) && $form['send']->isSubmittedBy()) ){
		
			if(empty($values->editId)){
				$newsletter = new Newsletter();
			}else{
				$newsletter = $this->service->getMessage($values->editId);
			}
			
			$newsletter->setSubject($values->subject);
			$newsletter->setBody($values->body);
			
			if(isset($form['send']) && $form['send']->isSubmittedBy()){
				$newsletter->setStatus(Newsletter::SENT);
				$newsletter->setSent(new DateTime());
			}else{
				$newsletter->setStatus(Newsletter::DRAFT);
			}
			
			$files = array();
			if(!empty($values->attachments)){
				foreach($values->attachments as $fileId){
					$files[] = $this->filesystem->getFileById($fileId);
				}
			}
			$newsletter->setAttachments($files);

			$this->service->saveMessage($newsletter);
			
			if(isset($form['send']) && $form['send']->isSubmittedBy()){
				
			}
		}
		
		$this->redirect('this');
	}
	
	protected function sendMails(Newsletter $message){
		
		$mails = $this->service->getAddresses(NULL, NULL);
		$mailer = new SendmailMailer();
		$mail = new Message();
		
		$mail->setSubject($message->subject);
		$mail->setEncoding('utf-8');
		$mail->setContentType('text/html');
		$mail->setHtmlBody($message->body);
		
		/*if(isset($message->attachments) && count($message->attachments) > 0){
			foreach($message->attachments as $file){
				
				$mail->addAttachment();
			}
		}*/
		
		
		
	}
	
}

<?php
namespace Kiwi\Newsletter\Be;

use Nette\Application\UI\Form;

/**
 * Description of NewsletterManagementControl
 *
 * @author basnik
 */
class NewsletterManagementControl extends \Nette\Application\UI\Control {
	
	/** @var \Kiwi\Newsletter\NewsletterService */
	protected $service;
	
	public function __construct(\Kiwi\Newsletter\NewsletterService $service, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct($parent, $name);
		
		$this->service = $service;
	}
	
	public function handleRemoveAddress($id){
		
		$address = $this->service->getAddressById($id);
		if(!$address){
			$this->flashMessage('Adresa nenalezena.', 'error');
		}else{
			$this->service->removeAddress($address);
			$this->flashMessage(sprintf('Adresa %s byla odebrána.', $address->email), 'success');
		}
		
		$this->redirect('this');
	}
	
	public function render(){
		
		// get newsletter statistics
		$this->template->statistics = $this->service->getStatistics();
		
		// show last 5 addresses
		$this->template->latest = $this->service->getAddresses(0, 5);
		
		$this->template->setFile(__DIR__.'/../templates/components/newsletterManagement.latte');
		$this->template->render();
	}
	
	protected function createComponentActionsForm(){
		
		$form = new Form();
		
		$form->addSubmit('export', 'Exportovat')
				->setValidationScope(FALSE);
		
		$form->addText('email', 'E-mail')
				->setRequired('Zadejte adresu k vyhledání.')
				->addRule(Form::EMAIL, 'E-mail není ve správném tvaru.');
		$form->addSubmit('search', 'Vyhledat adresu');
		
		$form->addSubmit('hideResults', 'Skrýt výsledek hledání')
				->setValidationScope(FALSE);
		
		$form->addProtection();
		
		$form->onSuccess[] = $this->proccessActionsForm;
		
		return $form;
	}
	
	public function proccessActionsForm(Form $form, $values){
		
		if($form['export']->isSubmittedBy()){
			$addresses = $this->service->getAddresses(NULL, NULL);
			$exportData = array();
			foreach($addresses as $address){
				$exportData[] = $address->email;
			}

			$response = new \Kiwi\Newsletter\MailExportResponse(implode(',', $exportData), 'mails.txt');
			$this->presenter->sendResponse($response);
			
		}else if($form['search']->isSubmittedBy()){
			
			$address = $this->service->getAddress($values->email);
			if($address){
				$this->template->foundAddress = $address;
			}else{
				$this->flashMessage(sprintf('Adresa %s nebyla v seznamu nalezena.', $values->email), 'warn');
			}
		}else if($form['hideResults']->isSubmittedBy()){
			$this->redirect('this');
			
		}
	}
	
}

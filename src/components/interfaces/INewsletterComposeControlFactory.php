<?php

namespace Kiwi\Newsletter\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface INewsletterComposeControlFactory {
	
	/** @return NewsletterComposeControl */
	public function create();
}

<?php

namespace Kiwi\Newsletter\Fe;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface INewsletterAddControlFactory {
	
	/** @return NewsletterAddControl */
	public function create();
}

<?php
namespace Kiwi\Newsletter\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface INewsletterGridFactory {
	
	/** @return NewsletterGrid */
	public function create();
}

<?php

namespace Kiwi\Newsletter\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface INewsletterManagementControlFactory {
	
	/** @return NewsletterManagementControl */
	public function create();
}

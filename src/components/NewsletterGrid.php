<?php
namespace Kiwi\Newsletter\Be;

use Doctrine\Common\Collections\Criteria;
use Kiwi\Newsletter\Entities\Newsletter;

/**
 * Description of NewsletterGrid
 *
 * @author basnik
 */
class NewsletterGrid extends \Kiwi\Grid{
	
	/** @var \Kiwi\Newsletter\NewsletterService */
	protected $service;
	
	public function __construct(\Kiwi\Newsletter\NewsletterService $service, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct($parent, $name);
		
		$this->service = $service;
		
		// construct the grid itself
		
		$this->setRowPrimaryKey('id');
		$this->addCellsTemplate(__DIR__.'/../templates/components/newsletterGrid.latte');

		$this->addColumn('subject', 'Předmět')->enableSort();
		$this->addColumn('status', 'Odeslán')->enableSort();

		$this->setFilterFormFactory(function() {
			
			$form = new \Nette\Forms\Container();
			$form->addText('subject');
			$form->addSelect('status', NULL, array(
				'1' => 'Vše',
				'2' => 'Rozepsané',
				'3' => 'Odeslané'
			));

			$form->addSubmit('filter', 'Filtrovat')->getControlPrototype()->class = 'btn btn-primary';
			$form->addSubmit('cancel', 'Zrušit filtr')->getControlPrototype()->class = 'btn';

			return $form;
		});
		
		$this->setDataSourceCallback($this->dataCallback);
	}
	
	public function dataCallback(array $filter, array $order=NULL, \Nette\Utils\Paginator $paginator=NULL) {
		
		// proccess filter
		$criteria = Criteria::create();
		$expr = Criteria::expr();
		if(!empty($filter)){
			foreach($filter as $column=>$val){
				if($column == 'subject'){
					$criteria->andWhere($expr->contains($column, $val));
				}else if($column == 'status'){
					if($val == 2){
						$criteria->andWhere($expr->eq($column, Newsletter::DRAFT));
					}else if($val == 3){
						$criteria->andWhere($expr->eq($column, Newsletter::SENT));
					}
				}else{
					$criteria->andWhere($expr->eq($column, $val));
				}
			}
		}
		
		if($order !== NULL){
			$criteria->orderBy(array($order[0] => $order[1]));
		}

		return $this->service->getMessages($criteria);
	}
	
	public function render() {
		$this->template->parentComponent = $this->getParent();
		parent::render();
	}
	
}

<?php
namespace Kiwi\Newsletter\Fe;

use Nette\Application\UI\Form;
use Kiwi\Newsletter\Entities\Address;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

/**
 * Description of NewsletterAddControl
 *
 * @author basnik
 */
class NewsletterAddControl extends \Nette\Application\UI\Control{
	
	/** @var \Kiwi\Newsletter\NewsletterService */
	protected $service;
	
	public function __construct(\Kiwi\Newsletter\NewsletterService $service, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		parent::__construct($parent, $name);
		
		$this->service = $service;
	}

	public function render(){
		
		$this->template->setFile(__DIR__.'/../templates/components/newsletterAdd.latte');
		$this->template->render();
	}
	
	/**
	 * Form pro subscribe.
	 * 
	 * @return Form
	 */
	protected function createComponentSubscribeForm(){
		
		$form = new Form();
		
		$form->addText('mail')
				->setRequired('Zadejte svůj e-mail.')
				->addRule(Form::EMAIL, 'Zadejte validní e-mail.');
		
		$form->addSubmit('subscribe', 'Přihlásit');
		$form->addProtection();
		
		$form->onSuccess[] = $this->proccessSubscribe;
		
		return $form;
	}
	
	/**
	 * Zpracovani subscribe formu
	 */
	public function proccessSubscribe(Form $form, $values){
		
		// does the address aleady exist?
		$address = $this->service->getAddress($values->mail);
		if($address === NULL){
			
			// save the entity
			$address = new Address();
			$address->setEmail($values->mail);
			$address->setSubscribed(new \Nette\Utils\DateTime());
			$address->setIsConfirmed(0);

			$this->service->saveAddress($address);
		}

		// send confirmation email (even if already subscribed - silently ignore that)
		try{
			$message = new Message();
			$message->setFrom('kiwi@iocko.cz');
			$message->setSubject('Registrace do newsletteru - potvrzení adresy');
			$message->setBody('Pro potvrzení účasti v newsletteru klikněte na následující odkaz:\n'. $this->presenter->link('//:Kiwi:Newsletter:Fe:Confirm:confirmSubscribe', sha1($address->id)));
			$message->addTo($values->mail);

			$mailer = new SendmailMailer;
			$mailer->send($message);
			
			$this->flashMessage('Na zadanou adresu byl odeslán e-mail s odkazem. Kliknutím na odkaz akci dokončíte.', 'warn');
			
			$this->redirect('this');
		
		}catch(\Nette\Application\AbortException $ae){
			// let it pass ...
			throw $ae;
			
		}catch(\Exception $e){
			
			$this->flashMessage('Odeslání e-mailu s potvrzením selhalo.', 'error');
			\Tracy\Debugger::log($e, \Tracy\Debugger::EXCEPTION);
		}
	}
}

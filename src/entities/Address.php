<?php

namespace Kiwi\Newsletter\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * This is an address subscribed to newsletter
 * @ORM\Entity
 * @ORM\Table(name="kw_newsletter_address")
 */
class Address extends \Kdyby\Doctrine\Entities\BaseEntity{

	/**
	 * Main identificator. Can also be used as a mark to retrieve data.
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;


	/**
	 * E-mail address itself.
	 * @ORM\Column(type="string", length=255)
	 */
	protected $email;

	
	/**
	 * States whether the address was confirmed. Unconfirmed addresses shall recieve no e-mails.
	 * @ORM\Column(type="boolean", name="is_confirmed", options={"default": 0})
	 */
	protected $isConfirmed;
	
	
	/**
	 * Subscribe date.
	 * @ORM\Column(type="datetime")
	 */
	protected $subscribed;
	

}

<?php

namespace Kiwi\Newsletter\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This is a newsletter message
 * @ORM\Entity
 * @ORM\Table(name="kw_newsletter")
 */
class Newsletter extends \Kdyby\Doctrine\Entities\BaseEntity{
	
	const DRAFT = 'draft';
	const SENT = 'sent';
	

	/**
	 * Main identificator. Can also be used as a mark to retrieve data.
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;


	/**
	 * Message subject
	 * @ORM\Column(type="string", length=255)
	 */
	protected $subject;
	
	/**
	 * Message body
	 * @ORM\Column(type="string")
	 */
	protected $body;
	
	/**
	* @ORM\ManyToMany(targetEntity="\Kiwi\Entities\File")
	* @ORM\JoinTable(name="kw_newsletter_files",
	*      joinColumns={@ORM\JoinColumn(name="newsletter_id", referencedColumnName="id")},
	*      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
	*      )
	*/
	protected $attachments;

	
	/**
	 * States whether the address was confirmed. Unconfirmed addresses shall recieve no e-mails.
	 * @ORM\Column(type="string", columnDefinition="ENUM('draft', 'sent')", options={"default": "draft"}) 
	 */
	protected $status;
	
	
	/**
	 * Sent date
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $sent;
	
	/**
	 * Sets attachments
	 * 
	 * @param array $attachments
	 */
	public function setAttachments(array $attachments){
		$this->attachments = new ArrayCollection($attachments);
	}

}

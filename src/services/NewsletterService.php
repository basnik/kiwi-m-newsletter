<?php
namespace Kiwi\Newsletter;

use Nette\Utils\DateTime;
use Nette\Utils\ArrayHash;
use Doctrine\Common\Collections\Criteria;

/**
 * This manages all newsletter data.
 *
 * @author basnik
 */
class NewsletterService extends \Nette\Object {
	
	/** @var \Kdyby\Doctrine\EntityManager */
	protected $em;
	
	/** @var \Kdyby\Doctrine\EntityRepository */
	protected $addresses;
	
	/** @var \Kdyby\Doctrine\EntityRepository */
	protected $messages;
	
	public function __construct(\Kdyby\Doctrine\EntityManager $em) {
		
		$this->em = $em;
		$this->addresses = $this->em->getRepository(Entities\Address::getClassName());
		$this->messages = $this->em->getRepository(Entities\Newsletter::getClassName());
	}
	
	/**
	 * Gets address by its sha1 id hash. If no address found, returns NULL.
	 * 
	 * @param string $hash
	 * @return Entities\Address|NULL
	 */
	public function getAddressByHash($hash){
		
		$builder = $this->em->createQueryBuilder();
		$builder->select('a');
		$builder->from(Entities\Address::getClassName() , 'a');
		$builder->andWhere('sha1(a.id) = :hash')
				->setParameter('hash', $hash);

		return $builder->getQuery()->getOneOrNullResult();
	}
	
	/**
	 * Gets address by its id. If no address found, returns NULL.
	 * 
	 * @param int $id
	 * @return Entities\Address|NULL
	 */
	public function getAddressById($id){
		
		return $this->addresses->find($id);
	}
	
	/**
	 * Gets address by its e-mail. If no address found, returns NULL.
	 * 
	 * @param string $email
	 * @return Entities\Address|NULL
	 */
	public function getAddress($email){
		
		return $this->addresses->findOneBy(array('email' => $email));
	}
	
	/**
	 * Gets last confirmed addresses.
	 * 
	 * @param int $offset Set NULL to omit
	 * @param int $limit Set NULL to omit
	 */
	public function getAddresses($offset=0, $limit=100){
		
		return $this->addresses->findBy(array('isConfirmed' => 1), array('subscribed ' => 'DESC'), $limit, $offset);
	}
	
	/**
	 * Adds new address to newsletter.
	 * @param \Kiwi\Newsletter\Entities\Address $address
	 */
	public function saveAddress(Entities\Address $address){
		$this->em->persist($address);
		$this->em->flush($address);
	}
	
	/**
	 * Removes given address from newsletter (unsubscribe).
	 * @param \Kiwi\Newsletter\Entities\Address $address
	 */
	public function removeAddress(Entities\Address $address){
		$this->em->remove($address);
		$this->em->flush($address);
	}
	
	/**
	 * Returns statistics as array:
	 *  - numAddresses - number of confirmed addresses
	 *  - numAddressesUnconfirmed - number of unconfirmed addresses
	 *  - lastAddressAdded - datetime when last address was added (even unconfirmed)
	 * 
	 *  @return \Nette\Utils\ArrayHash
	 */
	public function getStatistics(){
		
		$statistics = array(
			'numAddresses' => $this->addresses->countBy(array('isConfirmed' => 1)),
			'numAddressesUnconfirmed' => $this->addresses->countBy(array('isConfirmed' => 0))
		);

		$query = $this->em->createQuery('SELECT MAX(a.subscribed) FROM Kiwi\Newsletter\Entities\Address a');
		$result = $query->getSingleScalarResult();
		$statistics['lastAddressAdded'] = $result === NULL ? NULL : new DateTime($result);

		return ArrayHash::from($statistics);
	}
	
	/**
	 * Gets all messages fullfilling given criteria.
	 * 
	 * @param Criteria $criteria
	 */
	public function getMessages(Criteria $criteria=NULL){
		return $this->messages->matching($criteria);
	}
	
	/**
	 * Gets one message by its id.
	 * 
	 * @param int $id
	 */
	public function getMessage($id){
		return $this->messages->find($id);
	}
	
	/**
	 * Saves newsletter message to database.
	 * @param \Kiwi\Newsletter\Entities\Newsletter $newsletter
	 */
	public function saveMessage(Entities\Newsletter $newsletter){
		$this->em->persist($newsletter);
		$this->em->flush($newsletter);
	}
	
	/**
	 * Removes given message.
	 * @param \Kiwi\Newsletter\Entities\Newsletter $message
	 */
	public function removeMessage(Entities\Newsletter $message){
		$this->em->remove($message);
		$this->em->flush($message);
	}
	
}

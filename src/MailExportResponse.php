<?php
namespace Kiwi\Newsletter;

/**
 * Description of MailExportResponse
 *
 * @author basnik
 */
class MailExportResponse extends \Nette\Object implements \Nette\Application\IResponse{
	
	/** @var string */
	protected $content;
	
	/** @var string */
	protected $fileName;
	
	/**
	 * 
	 * @param string $content File content in string
	 * @param string $fileName Filename for output
	 */
	public function __construct($content, $fileName) {
		
		$this->content = $content;
		$this->fileName = $fileName;
	}
	
	
	public function send(\Nette\Http\IRequest $httpRequest, \Nette\Http\IResponse $httpResponse) {
		
		$httpResponse->setContentType('text/plain');
		$httpResponse->setHeader('Content-Disposition', 'attachment; filename="'.$this->fileName.'"; filename*=utf-8\'\''.rawurlencode($this->fileName));
		$httpResponse->setHeader('Content-Length', mb_strlen($this->content, 'utf-8'));
		
		echo $this->content;
	}

}

<?php
namespace Kiwi\Newsletter;

/**
 *Sets up newsletter
 *
 * @author basnik
 */
class Extension extends \Nette\DI\CompilerExtension implements \Kdyby\Doctrine\DI\IEntityProvider{
	
	/**
	 * Add our services
	 */
	public function loadConfiguration() {
		
		$builder = $this->getContainerBuilder();
		
		// register our services
		$builder->addDefinition($this->prefix('main'))->setClass('\Kiwi\Newsletter\NewsletterService');
		$builder->addDefinition($this->prefix('feAddControl'))->setImplement('Kiwi\Newsletter\Fe\INewsletterAddControlFactory');
		$builder->addDefinition($this->prefix('beManagementControl'))->setImplement('Kiwi\Newsletter\Be\INewsletterManagementControlFactory');
		$builder->addDefinition($this->prefix('beComposeControl'))->setImplement('Kiwi\Newsletter\Be\INewsletterComposeControlFactory');
		$builder->addDefinition($this->prefix('beNewsletterGrid'))->setImplement('Kiwi\Newsletter\Be\INewsletterGridFactory');
		
		// define route for confirmation
		$builder->addDefinition($this->prefix('routeNewsletterConfirm'))
			->setClass('\Nette\Application\Routers\Route', array('newsletter-actions/[<action>[/<id>]]', 'Kiwi:Newsletter:Fe:Confirm:'))
			->setAutowired(FALSE)
			->setInject(FALSE);
		
		// add our backend route to application router
		$routerService = $builder->getDefinition('router');
		$routerService->addSetup('\Kiwi\KiwiExtension::prependRoute($service, ?)', [$this->prefix('@routeNewsletterConfirm')]);
	}

	/**
	 * Register newsletter doctrine entities
	 */
	public function getEntityMappings() {
		return array( 'Kiwi\Newsletter' => __DIR__.'/entities' );
	}
	
}
